# Spring Batch Hello World Project

## Project Description

This is a simple Spring Batch hello world project designed to demonstrate the basic usage of Spring Batch for processing
data from a CSV file and storing it in an H2 in-memory database.

## Prerequisites

Before you begin, ensure you have the following installed:

- Java Development Kit (JDK) installed
- Git installed
- An Integrated Development Environment (IDE) such as IntelliJ IDEA or Eclipse
- Maven installed (for dependency management)

## Getting Started

1. **Clone the Repository:**

   ```bash
   git clone https://gitlab.com/techtalk5782345/spring-batch-example.git

2. Navigate to the project directory:
   ```
    cd spring-batch-example
   ```
3. Run the Spring Boot Application:
   ```
   ./mvnw spring-boot:run
   ```
   The application will start, and the Spring Batch job will be executed automatically. You can also trigger the job
   manually by accessing the following URL:
   ```
   http://localhost:8080/startBatch
   ```

## Access the H2 In-Memory Database

Navigate to the H2 console for database interactions:

URL: ``http://localhost:8080/h2-console``
Use the credentials specified in ``application.properties``