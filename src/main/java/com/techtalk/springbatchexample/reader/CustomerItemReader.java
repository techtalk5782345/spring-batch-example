package com.techtalk.springbatchexample.reader;

import com.techtalk.springbatchexample.model.Customer;
import org.springframework.batch.item.ItemReader;
import org.springframework.batch.item.file.builder.FlatFileItemReaderBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;

public class CustomerItemReader {

    @Bean
    public ItemReader<Customer> customerItemReader() {
        return new FlatFileItemReaderBuilder<Customer>()
                .name("customerItemReader")
                .resource(new ClassPathResource("customers.csv"))
                .delimited()
                .names("name", "age", "country")
                .targetType(Customer.class)
                .build();
    }
}
