package com.techtalk.springbatchexample.model;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;

@Entity
@Data
public class Customer {
    @Id
    private Long id;
    private String name;
    private int age;
    private String country;
    private String email;
}

//https://www.youtube.com/watch?v=TmY686-2FTg&list=PLrlbnvtRPsstK9ingVQfYgs5x6_9US6hH
