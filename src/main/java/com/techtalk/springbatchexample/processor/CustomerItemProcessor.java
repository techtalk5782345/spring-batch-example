package com.techtalk.springbatchexample.processor;

import com.techtalk.springbatchexample.model.Customer;
import org.springframework.batch.item.ItemProcessor;

public class CustomerItemProcessor implements ItemProcessor<Customer, Customer> {

    @Override
    public Customer process(Customer customer) {
        // Implementing filter based on country (e.g., filter out customers from "Canada")
        if ("Canada".equals(customer.getCountry())) {
            // Returning null to indicate that the item should be skipped
            return null;
        }

        // Add any additional processing logic here

        return customer;
    }
}
