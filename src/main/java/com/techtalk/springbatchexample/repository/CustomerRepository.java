package com.techtalk.springbatchexample.repository;

import com.techtalk.springbatchexample.model.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
