package com.techtalk.springbatchexample.writer;

import com.techtalk.springbatchexample.model.Customer;
import com.techtalk.springbatchexample.repository.CustomerRepository;
import org.springframework.batch.item.Chunk;
import org.springframework.batch.item.ItemWriter;

public class CustomerItemWriter implements ItemWriter<Customer> {

    private final CustomerRepository customerRepository;

    public CustomerItemWriter(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    @Override
    public void write(Chunk<? extends Customer> chunk) throws Exception {
        customerRepository.saveAll(chunk);
    }
}
